<?php

namespace App\Exports;

use App\Page;
use App\Fishpond;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KhawzawlExport implements FromCollection,WithHeadings
{
    public function headings(): array {
        return [
            'id','name','Father name','Contact','Address','District','Location of pond','Tehsil','Image','Area','Epic/Adhaar','Name of scheme','pond Image one','pond Image two','pond Image three','pond Image four','Latitude','Longitude',
        ];
      }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Fishpond::all()->where('district','Khawzawl');
    }
}
