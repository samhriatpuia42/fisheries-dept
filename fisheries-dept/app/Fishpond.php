<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Fishpond extends Model
{
    protected $fillable=[
        'district','image','pondImage_one',
    ];

     // Fetch all users
   public static function getFishponds(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();

      return $records;
      // ->where('outsidestate','Assam')
    }

    public static function getAizawl(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->where('district','Aizawl')->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
      // ->where('outsidestate','Assam')
    }
    public static function getKolasib(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->where('district','Kolasib')->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
      // ->where('outsidestate','Assam')
    }
    public static function getLawngtlai(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getlunglei(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getMamit(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getSiaha(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getSerchhip(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getChamphai(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getHnahthial(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }

    public static function getSaitual(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }


    public static function getKhawzawl(){
      $records = DB::table('fishponds')
      ->select('id','name','fname','contact','address','district','location_of_pond','tehsil','image','area','epic_no','name_of_scheme','pondImage_one','pondImage_two','pondImage_three','pondImage_four','lat','lng')
      ->orderBy('id', 'asc')
      ->get()
      ->toArray();
      return $records;
    }
}
