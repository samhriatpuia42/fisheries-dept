<?php

namespace App\Http\Controllers;
use App\Fishpond;
use App\Location;
use App\User;
use App\Scheme;
use App\Tehsil;
use GuzzleHttp\Client;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FishpondController extends Controller
{
    public function store(Request $request)
    {
        $fishpond = new Fishpond();

        $this->validate($request, [
            'district' => '',
            'image' => '',
            'fname'=>'',
            'address'=>'',
            'location_of_pond'=>'',
            'tehsil'=>'',
            'area'=>'',
            'epic_no'=>'',
            'name_of_scheme'=>'',
            'lat'=>'',
            'lng'=>'',

        ]);
        //for single images
        error_log($request->file('pondImage_one'));

        if ($files = $request->file('image')) {
            $destinationPath = 'public/image/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $fishpond->image = $profileImage;
         }

         if ($files = $request->file('pondImage_one')) {
            $destinationPath1 = 'public/image1/'; // upload path
            $pi1 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath1, $pi1);
            $fishpond->pondImage_one=$pi1;
         }
         if ($files = $request->file('pondImage_two')) {
            $destinationPath2 = 'public/image2/'; // upload path
            $pi2 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath2, $pi2);
            $fishpond->pondImage_two=$pi2;
         }

         if ($files = $request->file('pondImage_three')) {
            $destinationPath3 = 'public/image3/'; // upload path
            $pi3 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath3, $pi3);
            $fishpond->pondImage_three=$pi3;
         }

         if ($files = $request->file('pondImage_four')) {
            $destinationPath4 = 'public/image4/'; // upload path
            $pi4 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath4, $pi4);
            $fishpond->pondImage_four=$pi4;
         }
         

         
        //DB UPLOAD FOR SINGLE PICTURE i.e Profile picture 
        
        $fishpond->district = $request->district;
        $fishpond->name= $request->name;
        $fishpond->contact= $request->contact;

        $fishpond->fname = $request->fname;
        $fishpond->address = $request->address;
        $fishpond->location_of_pond = $request->location_of_pond;
        $fishpond->tehsil = $request->tehsil;
        $fishpond->area = $request->area;
        $fishpond->epic_no = $request->epic_no;
        $fishpond->name_of_scheme = $request->name_of_scheme;
        
        // $fishpond->pondImage_one=$pi1;
        // $fishpond->pondImage_two=$pi2;
        // $fishpond->pondImage_three=$pi3;
        // $fishpond->pondImage_four=$pi4;

        // if ($files = $request->file('pondImage_one')) {
        //     $fishpond->pondImage_one=$pi1;
        //  }
        //  if ($files = $request->file('pondImage_two')) {
        //      $fishpond->pondImage_two=$pi2;
        //  }
        //  if ($files = $request->file('pondImage_three')) {
        //     $fishpond->pondImage_three=$pi3;
        //  }
        //  if ($files = $request->file('pondImage_four')) {
        //      $fishpond->pondImage_two=$pi4;
        //  }

        $fishpond->lat = $request->lat;
        $fishpond->lng = $request->lng;
        $fishpond->user_id=$request->user_id;
      
        $fishpond->save();
        
        if (auth()->user()){
            
            return response()->json([
                'success' => true,
                'data' => $fishpond->toArray()
            ]);
        }
            
        else
            return response()->json([
                'success' => false,
                'message' => 'Fishpond could not be added'
            ], 500);
       
    }


    public function editUserData(Request $request, $id)
    {
       
       $fishpond = Fishpond::find($id);

      // error_log("THis is gonna changed the world:"+$request->file('pondImage_one'));
    //    for single images
        if ($files = $request->file('image')) {
            $destinationPath = 'public/image/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $fishpond->image = $profileImage;
           // $insert['image'] = "$profileImage";
         }

         
         if ($files = $request->file('pondImage_one')) {
            $destinationPath1 = 'public/image1/'; // upload path
            $pi1 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath1, $pi1);
            $fishpond->pondImage_one=$pi1;
         }
         if ($files = $request->file('pondImage_two')) {
            $destinationPath2 = 'public/image2/'; // upload path
            $pi2 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath2, $pi2);
            $fishpond->pondImage_two=$pi2;
         }

         if ($files = $request->file('pondImage_three')) {
            $destinationPath3 = 'public/image3/'; // upload path
            $pi3 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath3, $pi3);
            $fishpond->pondImage_three=$pi3;
         }

         if ($files = $request->file('pondImage_four')) {
            $destinationPath4 = 'public/image4/'; // upload path
            $pi4 = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath4, $pi4);
            $fishpond->pondImage_four=$pi4;
         }

        // UPLOAD
        if(!empty($request->district))
            $fishpond->district = $request->district;
        if(!empty($request->fname))
            $fishpond->fname = $request->fname;
        // if(!empty($request->name))
        //     $fishpond->name=$request->name;
        if(!empty($request->address))    
            $fishpond->address = $request->address;
        if(!empty($request->location_of_pond)) 
            $fishpond->location_of_pond = $request->location_of_pond;
        if(!empty($request->tehsil)) 
            $fishpond->tehsil = $request->tehsil;
        if(!empty($request->area))
            $fishpond->area = $request->area;
        if(!empty($request->epic_no))
            $fishpond->epic_no = $request->epic_no;
        if(!empty($request->name_of_scheme))
            $fishpond->name_of_scheme = $request->name_of_scheme;

        if(!empty($request->lat))
            $fishpond->lat = $request->lat;
        if(!empty($request->lng))
            $fishpond->lng = $request->lng;
        
        $fishpond->save();


        if (auth()->user())
            return response()->json([
                'success' => true,
                'data' => $fishpond->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Fishpond could not be added'
            ], 500);
        
    }


    public function pondList()
    {
        $pondList=Fishpond::all();
        return response()->json([
            'data' => $pondList,
            'message' => 'success'
        ], 500);
    }



    public function search(Request $request)
    {

        //REMAKE BY LTP 10 JUL,2020
        $inputDistrict=$request->district;
        $inputScheme=$request->scheme;
        
        $districtArr = explode(",",$inputDistrict);
        $schemeArr = explode(",", $inputScheme);

       
        

        $searchresult=Fishpond::orderBy('district','ASC')
        ->where(function($q) use($districtArr){

                foreach($districtArr as $district){
                    $q->orWhere('district','LIKE','%'.$district.'%');
                }})
        ->where(function($q) use ($schemeArr){
                foreach($schemeArr as $scheme){
                    $q->orWhere('name_of_scheme','LIKE','%'.$scheme.'%');
                }
            })->get();
                
        error_log($searchresult);
            return response()->json([
                'data' => $searchresult,
                'message' => 'success'
            ], 500);
    }



    public function searchPonds(Request $request)
    {
        $ponds=Fishpond::get();
        return $ponds;
    }

    public function searchCity(Request $request)
    {
        $locationVal=$request->locationVal;
        $matchedTehsils=Fishpond::where('tehsil','like',"%$locationVal%")->pluck('tehsil','tehsil');
        //return response()->json(['items'=>$matchedTehsils]);
        return view('ajxresult',compact('matchedTehsils'));
    }

    public function getAll()
    {
        $ponds=DB::table('fishponds')->get();
        // dd($girls);
        return response()->json($ponds);
    }

    public function searchTehsil(Request $request)
    {
        $distval=$request->distval;
        $matchedTehsils=Location::where('district',$distval)->pluck('tehsil','tehsil');

        return view('ajaxresult',['matchedTehsils'=>$matchedTehsils]);
    }

    public function searchPondsAizawl(){
        $ponds=Fishpond::where('district','aizawl')->get();
        return $ponds;  
    }

    public function searchPondsKolasib(){
        $kponds=Fishpond::where('district','kolasib')->get();
        return $kponds;  
    }

    public function searchPondsLawngtlai(){
        $lponds=Fishpond::where('district','lawngtlai')->get();
        return $lponds;  
    }

    public function searchPondsLunglei(){
        $ponds=Fishpond::where('district','lunglei')->get();
        return $ponds;  
    }

    public function searchPondsMamit(){
        $mponds=Fishpond::where('district','mamit')->get();
        return $mponds;  
    }

    public function searchPondsSiaha(){
        $sa_ponds=Fishpond::where('district','siaha')->get();
        return $sa_ponds;  
    }

    public function searchPondsserchhip(){
        $ser_ponds=Fishpond::where('district','serchhip')->get();
        return $ser_ponds;  
    }

    public function searchPondsChamphai(){
        $ch_ponds=Fishpond::where('district','champhai')->get();
        return $ch_ponds;  
    }

    public function searchPondsHnahthial(){
        $hponds=Fishpond::where('district','hnahthial')->get();
        return $hponds;  
    }

    public function searchPondsSaitual(){
        $sai_ponds=Fishpond::where('district','saitual')->get();
        return $sai_ponds;  
    }

    public function searchPondsKhawzawl(){
        $kh_ponds=Fishpond::where('district','khawzawl')->get();
        return $kh_ponds;  
    }

    public function findPondWeb($id)
    {
        $pondDetail=Fishpond::findOrFail($id);
        return $pondDetail;
    }

    public function findPond($id)
    {
        //CHANGED BY TPA 21JUN20
        $pondDetail=Fishpond::where('user_id',$id)->first();

        error_log($pondDetail);
        if($pondDetail==null){
            error_log("null");
            return response()->json([
                'message' => false,
            ]); 
        }else{
            error_log("not null");
            return response()->json([
                'message' => true,
                'data' => $pondDetail
            ]);
        }  
    }

    // Consider this to delete
    public function findImages($id)
    {
        $data=Fishpond::find($id);
        $image_one = $data->pondImage_one;
        return $image_one;
    }

    public function findImage_one($id)
    {
        $data=Fishpond::find($id);
        $image_one = $data->pondImage_one;
        return $image_one;
    }

    public function findImage_two($id)
    {
        $data=Fishpond::find($id);
        $image_two = $data->pondImage_two;
        return $image_two;
    }

    public function findImage_three($id)
    {
        $data=Fishpond::find($id);
        $image_three = $data->pondImage_three;
        return $image_three;
    }

    public function findImage_four($id)
    {
        $data=Fishpond::find($id);
        $image_four = $data->pondImage_four;
        return $image_four;
    }

    public function findPropic($id)
    {
        $data=Fishpond::find($id);
        $image=$data->image;
        return $image;
    }

    //Created by TPA 26June20
    public function myScheme()
    {
        error_log("Scheme add");
        $myScheme = Scheme::all();
        return $myScheme;
    }

    //Created by TPA 26June20
    public function myTehsil()
    {
        $myTehsil = Tehsil::all();
        return $myTehsil;
    }

    //Created by TPA 26June20
    public function selectedDistrict($district){
        error_log($district);

        $myDistrict = Fishpond::where('district',$district)->get();
        error_log($myDistrict);
        if($myDistrict!=null) return $myDistrict;
        else return response()->json([
            'message' => false, 
        ]);
    }

    //Created by TPA 29Jun20
    public function selectedScheme($selectedScheme){

        //1. Explode the scheme
        //2. create a somesort of nested where querey
        //3. return the object
        $selectedSchemeArr = explode(",",$selectedScheme);
    }

    //CREATED BY TPA 11Jul20,
    public function sendSMSAPI(Request $request)
    {
        $response="";
        error_log($request->message);
        $tehsilContactArr=Fishpond::where('tehsil','=',$request->tehsil)->pluck('contact')->toArray();
        $message=$request->message;

        $contactArrChunk=array_chunk($tehsilContactArr,30,true);
        foreach($contactArrChunk as $phoneArr)
        {
            $phones=implode(',',$phoneArr);
            $client = new Client();
            $response=$client->request ('POST','https://sms.mizoram.gov.in/api', [
                'form_params' => [
                'api_key' => 'b53366c91585c976e6173e69f6916b2d',
                'number' => $phones,  
                'message' => $message,
                ]
            ]);
        }
        return $response->getBody()->getContents();
               //dd($tehsilContactArr);
        
    }
   //CREATED BY TPA 16Jul20,
    public function sendSingleSms(Request $request)
    {
        $response="";
        error_log($request->message);
       // $tehsilContactArr=Fishpond::where('tehsil','=',$request->tehsil)->pluck('contact')->toArray();
        $message=$request->message;
        $contact=$request->contact;

        //$contactArrChunk=array_chunk($tehsilContactArr,30,true);
       // foreach($contactArrChunk as $phoneArr)
       // {
         //   $phones=implode(',',$phoneArr);
            $client = new Client();
            $response=$client->request ('POST','https://sms.mizoram.gov.in/api', [
                'form_params' => [
                'api_key' => 'b53366c91585c976e6173e69f6916b2d',
                'number' => $contact,  
                'message' => $message,
                ]
            ]);
      //  }
        return $response->getBody()->getContents();
               //dd($tehsilContactArr);
        
    }
    //CREATED BY TPA 11Jul20,
    public function searchByName($name){
        $myName = strtolower($name);
        $selectedFarmer = Fishpond::where('name','LIKE','%'.$myName.'%')->get();
        
        error_log("Seach by name:" . $selectedFarmer);
        if($selectedFarmer->isEmpty()
        ){
            error_log("empty");
            return response()->json([
                'message' => false
            ]);
        }else{
            error_log("not empty");
            return response()->json([
                'data' => $selectedFarmer,
                'message' => true
            ]);
        }
    }


}
