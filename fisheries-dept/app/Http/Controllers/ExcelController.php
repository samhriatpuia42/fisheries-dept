<?php

namespace App\Http\Controllers;

use App\Exports\MamitExport;
use App\Exports\SiahaExport;
use Illuminate\Http\Request;
use App\Exports\AizawlExport;
use App\Exports\KolasibExport;
use App\Exports\LungleiExport;
use App\Exports\SaitualExport;
use App\Exports\ChamphaiExport;
use App\Exports\FishpondExport;
use App\Exports\KhawzawlExport;
use App\Exports\SerchhipExport;
use App\Exports\HnahthialExport;
use App\Exports\LawngtlaiExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{

    public function index(){
        return view('excel.index');
     }
  
    public function exportExcel(Request $request){
           return Excel::download(new FishpondExport, 'fishpond.xlsx');
    }

    public function exportExcelAizawl(Request $request){
        return Excel::download(new AizawlExport, 'Aizawl.xlsx');
    }

    public function exportExcelkolasib(Request $request){
        return Excel::download(new KolasibExport, 'Kolasib.xlsx');
    }

    public function exportExcelLawngtlai(Request $request){
        return Excel::download(new LawngtlaiExport, 'Lawngtlai.xlsx');
    }

    public function exportExcelLunglei(Request $request){
        return Excel::download(new LungleiExport, 'lunglei.xlsx');
    }

    public function exportExcelMamit(Request $request){
        return Excel::download(new MamitExport, 'Mamit.xlsx');
    }

    public function exportExcelSiaha(Request $request){
        return Excel::download(new SiahaExport, 'Siaha.xlsx');
    }

    public function exportExcelSerchhip(Request $request){
        return Excel::download(new SerchhipExport, 'Serchhip.xlsx');
    }

    public function exportExcelChamphai(Request $request){
        return Excel::download(new ChamphaiExport, 'Champhai.xlsx');
    }

    public function exportExcelHnahthial(Request $request){
        return Excel::download(new HnahthialExport, 'Hnahthial.xlsx');
    }


    public function exportExcelSaitual(Request $request){
        return Excel::download(new SaitualExport, 'Saitual.xlsx');
    }

    public function exportExcelKhawzawl(Request $request){
        return Excel::download(new KhawzawlExport, 'Khawzawl.xlsx');
    }
    
}
