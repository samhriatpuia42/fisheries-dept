<?php

namespace App\Http\Controllers;
use App\Scheme;
use App\Tehsil;
use Illuminate\Http\Request;

class SchemesController extends Controller
{
    public function index()
    {
        $schemes=Scheme::paginate(12);
        $tehsils=Tehsil::all();
        return view('scheme.index',['scheme'=>$schemes,'tehsil'=>$tehsils]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'sname' => 'required',
            
        ]);
        // dd($request->district);
        $newScheme=new Scheme();
        $newScheme->sname=$request->sname;

        $newScheme->save();
        return back();
    }

    public function destroy($id)
    {
        $scheme=Scheme::findOrFail($id);
        $scheme->delete();
        return back();
    }
}
