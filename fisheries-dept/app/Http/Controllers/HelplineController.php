<?php

namespace App\Http\Controllers;

use App\Helpline;
use Illuminate\Http\Request;

class HelplineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helpline = Helpline::first();
        return $helpline;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Helpline  $helpline
     * @return \Illuminate\Http\Response
     */
    public function show(Helpline $helpline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Helpline  $helpline
     * @return \Illuminate\Http\Response
     */
    public function edit(Helpline $helpline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Helpline  $helpline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helpline $helpline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Helpline  $helpline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Helpline $helpline)
    {
        //
    }
}
