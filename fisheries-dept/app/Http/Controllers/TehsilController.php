<?php

namespace App\Http\Controllers;
use App\Tehsil;
use Illuminate\Http\Request;

class TehsilController extends Controller
{
    public function index()
    {
        $tehsils=Tehsil::paginate(12);
        return view('tehsil.index',['tehsil'=>$tehsils]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'tname' => 'required',
            'district' => 'required',
        ]);
        // dd($request->district);
        $newTehsil=new Tehsil();
        $newTehsil->tname=$request->tname;
        $newTehsil->district=$request->district;

        $newTehsil->save();
        return back();
    }

    public function destroy($id)
    {
        $tehsil=Tehsil::findOrFail($id);
        $tehsil->delete();
        return back();
    }
    
}
