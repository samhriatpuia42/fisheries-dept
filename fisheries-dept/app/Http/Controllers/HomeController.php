<?php

namespace App\Http\Controllers;
use App\User;
use App\Tehsil;
use App\Fishpond;
use App\Scheme;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\TabCompletion\Matcher\FunctionsMatcher;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function farmerList()
    {
        $farmers=Fishpond::paginate(5);
        $scheme=Scheme::all();

        return view('farmer_list',['farmers'=>$farmers,'schemes'=>$scheme]);
    }

    public function findFarmer(Request $request)
    {
        $farmer=Fishpond::findOrFail($request->id);
        return $farmer;
    }

    public function searchQuery(Request $request)
    {
        if($request->district!="" & $request->sname=="")
        {
            $farmers=Fishpond::where('district', '=', $request->district)->paginate(5);
            $scheme=Scheme::all();
            return view('farmer_list_searchQuery',['farmers'=>$farmers,'schemes'=>$scheme]);
        }
        
        if($request->district=="" & $request->sname!="")
        {
            $farmers=Fishpond::where('name_of_scheme', '=', $request->sname)->paginate(5);
            $scheme=Scheme::all();
            return view('farmer_list_searchQuery',['farmers'=>$farmers,'schemes'=>$scheme]);
        }
        if($request->district!="" & $request->sname!="")
        {
            $farmers=Fishpond::where('district', '=', $request->district)
                        ->where('name_of_scheme', '=', $request->sname)->paginate(5);
            $scheme=Scheme::all();
            return view('farmer_list_searchQuery',['farmers'=>$farmers,'schemes'=>$scheme]);
        }
    }

    public function find(Request $request)
    {
        // $farmer=Fishpond::findOrFail($request->contact);
        $farmer=DB::table('fishponds')->where('contact', $request->contact)->first();
        //  dd($request);
         //return redirect()->route('find',['farmer'=>$farmer]);
         $schemes=Scheme::all();
        if($farmer==NULL)
            abort(404);
        else
            return view('/find_farmer',['farmer'=>$farmer,'schemes'=>$schemes]);
    }

    public function applicationlist()
    {
        $applications=Fishpond::where('approve','0')->paginate(12);
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('applications',compact('users','applications','fishponds','applied','tehsils'));
        // return view('applications',['applications'=>$applications]);
    }
    public function viewDetails($id)
    {
        $fishpond=Fishpond::findOrFail($id);
        $applications=Fishpond::where('approve','0')->paginate(12);
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();

        return view('fishpond_detail',compact('users','applications','fishponds','applied','tehsils','fishpond'));
        // return view('fishpond_detail',['fishpond'=>$fishpond]);
    }

    public function approve($id)
    {
        $application=Fishpond::find($id);
        $application->approve='1';
        $application->save();
        $applications=Fishpond::where('approve','0')->paginate(12);
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('applications',compact('users','applications','fishponds','applied','tehsils'));
    }

    public function resubmit(Request $request, $id)
    {
        $request->validate([
            'reason' => 'required', 
        ]);
        $application=Fishpond::find($id);
        $application->approve='2';
        $application->reason=$request->reason;
        $application->save();
        $applications=Fishpond::where('approve','0')->paginate(12);
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        
        return view('applications',compact('users','applications','fishponds','applied','tehsils'));
    }

    public function resubmitResubmit($id)
    {
        $application=Fishpond::find($id);
        $application->approve='2';
        $application->save();
        $resubmit=Fishpond::where('approve','2')->get();
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('resubmit',compact('tehsils','resubmit','users','fishponds','applied'));
    }

    public function resubmitList()
    {
        $resubmit=Fishpond::where('approve','2')->get();
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('resubmit',compact('tehsils','resubmit','users','fishponds','applied'));
    }

    public function resubmitViewDetails($id)
    {   
        $fishpond=Fishpond::findOrFail($id);
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('resubmit_details',compact('fishpond','fishponds','users','applied','tehsils'));
    }
    public function approveResubmit($id)
    {
        $fishpond=Fishpond::findOrFail($id);
        $fishpond->approve='1';
        $fishpond->save();
        $resubmit=Fishpond::where('approve','2')->get();
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('resubmit',compact('tehsils','resubmit','users','fishponds','applied'));

    }

    public function sendSMS(Request $request)
    {
        error_log($request->message);
        $tehsilContactArr=Fishpond::where('tehsil','=',$request->tehsil)->pluck('contact')->toArray();
        $message=$request->message;

        $contactArrChunk=array_chunk($tehsilContactArr,30,true);
        foreach($contactArrChunk as $phoneArr)
        {
            $phones=implode(',',$phoneArr);
            $client = new Client();
            $response=$client->request ('POST','https://sms.mizoram.gov.in/api', [
                'form_params' => [
                'api_key' => 'b53366c91585c976e6173e69f6916b2d',
                'number' => $phones,  
                'message' => $message,
                ]
            ]);
            $applications=Fishpond::where('approve','0')->paginate(12);
            $users=User::count();
            $fishponds=Fishpond::where('approve','1')->count();
            $applied=Fishpond::count();
            $tehsils=Tehsil::all();
            return view('applications',compact('users','applications','fishponds','applied','tehsils'));
        }
        
        //dd($tehsilContactArr);
        
    }


    // editing farmer
    public function farmersEditList()
    {
        // dd("Test");
        $farmers=Fishpond::paginate(12);
        return view('editFarmerList',compact('farmers'));
    }

    public function farmeredit(Request $request)
    {
        $farmer=Fishpond::findOrFail($request->id);
        // dd($farmer);
        return view('editFarmer',compact('farmer'));
    }

    public function farmerEditSave(Request $request)
    {

        //dd($request->id);
        $fishpond=Fishpond::findOrFail($request->id);
        //dd($fishpond->user_id);
        // $fishpond->user_id=$request->user_id;
        $this->validate($request, [
            'district' => 'required',
            'name'=>'required',
            'image' => '',
            'fname'=>'required',
            'address'=>'required',
            'location_of_pond'=>'required',
            'tehsil'=>'required',
            'area'=>'required',
            'epic_no'=>'required',
            'name_of_scheme'=>'required',
            'lat'=>'required',
            'lng'=>'required',
            'user_id'=>'required',

        ]);
        $fishpond->district = request('district');
        $fishpond->name= request('name');
        $fishpond->contact= request('contact');
        $fishpond->fname = request('fname');
        $fishpond->address = request('address');
        $fishpond->location_of_pond = request('location_of_pond');
        $fishpond->tehsil = request('tehsil');
        $fishpond->area = request('area');
        $fishpond->epic_no = request('epic_no');
        $fishpond->name_of_scheme = request('name_of_scheme');
        $fishpond->lat = request('lat');
        $fishpond->lng = request('lng');   
        $fishpond->save();
        $farmers=Fishpond::paginate(5);
        return redirect()->route('farmerList',compact('farmers'));
    }


    public function destroy_pond_web($id)
    {
        $fishpond=Fishpond::findOrFail($id);
        $fishpond->delete();

        $resubmit=Fishpond::where('approve','2')->get();
        $users=User::count();
        $fishponds=Fishpond::where('approve','1')->count();
        $applied=Fishpond::count();
        $tehsils=Tehsil::all();
        return view('resubmit',compact('tehsils','resubmit','users','fishponds','applied'));
    }

}
