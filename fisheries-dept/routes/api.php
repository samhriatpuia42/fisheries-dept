<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'PassportController@login');
    Route::post('register', 'PassportController@register');
    
    Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
 
    Route::post('/fishponds/create', 'FishpondController@store');
    Route::put('/fishponds/uploadpond/{id}','FishpondController@uploadpond');
    // Edit data by user
    Route::put('/fishponds/edit/{id}','FishpondController@editUserData');
    Route::get('/fishponds/pondlist','FishpondController@pondList');
    Route::post('/fishponds/search','FishpondController@search');

    Route::get('schemes','SchemeController@listOfSchemes');

    Route::get('tehsils','TehsilController@listOfTehsils');
    Route::post('checkApproval/{id}','PassportController@checkApprove');
    Route::post('checkResubmit/{id}','PassportController@checkResubmit');
    Route::get('/selectedDistrict/{district}','FishpondController@selectedDistrict');    
});

// Check if user_id already exist
Route::post('/checkFishpondExist/{id}','PassportController@checkFishpondExist');

Route::post('/searchPonds','FishPondController@searchPonds');
Route::post('/searchTehsil','FishPondController@searchTehsil');

// district map routes
Route::post('/searchPondsAizawl','FishPondController@searchPondsAizawl');
Route::post('/searchPondsKolasib','FishPondController@searchPondsKolasib');
Route::post('/searchPondsLawngtlai','FishPondController@searchPondsLawngtlai');
Route::post('/searchPondsLunglei','FishPondController@searchPondsLunglei');
Route::post('/searchPondsMamit','FishPondController@searchPondsMamit');
Route::post('/searchPondsSiaha','FishPondController@searchPondsSiaha');
Route::post('/searchPondsSerchhip','FishPondController@searchPondsSerchhip');
Route::post('/searchPondsChamphai','FishPondController@searchPondsChamphai');
Route::post('/searchPondsHnahthial','FishPondController@searchPondsHnahthial');
Route::post('/searchPondsKhawzawl','FishPondController@searchPondsKhawzawl');
Route::post('/searchPondsSaitual','FishPondController@searchPondsSaitual');


Route::post('/findPond/{id}','FishPondController@findPond');
Route::post('/findPondWeb/{id}','FishPondController@findPondWeb');
Route::post('/findImages/{id}','FishPondController@findImages');
Route::post('/findPropic/{id}','FishPondController@findPropic');
Route::post('/findImage_one/{id}','FishPondController@findImage_one');
Route::post('/findImage_two/{id}','FishPondController@findImage_two');
Route::post('/findImage_three/{id}','FishPondController@findImage_three');
Route::post('/findImage_four/{id}','FishPondController@findImage_four');

// new
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/schemes','SchemesController@index')->name('scheme.index');
Route::get('schemes/edit/{id}')->name('scheme.edit');
Route::get('schemes/delete/{id}')->name('scheme.delete');
Route::post('schemes/create','SchemesController@store')->name('schemes.create');


Route::get('/tehsils','TehsilController@index')->name('tehsil.index');
Route::post('/tehsils/create','TehsilController@store')->name('tehsil.create');

//Created by TPA 26June20
Route::get('/myscheme','FishpondController@myScheme');
Route::get('/mytehsil','FishpondController@myTehsil');


// SMS Send
Route::post('/sms','HomeController@sendSMS');

//TPA
Route::post('/smsapi','FishpondController@sendSMSAPI');
Route::post('/singlesmsapi','FishpondController@sendSingleSms');

Route::get('/fishpond/searchbyname/{name}','FishpondController@searchByName');
Route::resource('helpline','HelplineController');