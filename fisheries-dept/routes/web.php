<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/map/all', function () {
    return view('map.map');
})->middleware('auth.basic');

Route::get('/map/aizawl',function(){
    return view('map.aizawl');
})->middleware('auth.basic');

Route::get('/map/kolasib',function(){
    return view('map.kolasib');
})->middleware('auth.basic');

Route::get('/map/lawngtlai',function(){
    return view('map.lawngtlai');
})->middleware('auth.basic');

Route::get('/map/lunglei',function(){
    return view('map.lunglei');
});

Route::get('/map/mamit',function(){
    return view('map.mamit');
})->middleware('auth.basic');

Route::get('/map/siaha',function(){
    return view('map.siaha');
})->middleware('auth.basic');

Route::get('/map/serchhip',function(){
    return view('map.serchhip');
})->middleware('auth.basic');

Route::get('/map/champhai',function(){
    return view('map.champhai');
})->middleware('auth.basic');

Route::get('/map/hnahthial',function(){
    return view('map.hnahthial');
})->middleware('auth.basic');

Route::get('/map/saitual',function(){
    return view('map.saitual');
})->middleware('auth.basic');

Route::get('/map/khawzawl',function(){
    return view('map.khawzawl');
})->middleware('auth.basic');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth.basic');

Route::get('/schemes','SchemesController@index')->name('schemes.index')->middleware('auth.basic');
Route::get('schemes/edit/{id}')->name('schemes.edit');
Route::delete('schemes/delete/{id}','SchemesController@destroy')->name('schemes.delete');
Route::post('schemes/create','SchemesController@store')->name('schemes.create');


Route::get('/tehsils','TehsilController@index')->name('tehsil.index');
Route::post('/tehsils/create','TehsilController@store')->name('tehsil.create');
Route::delete('/tehsils/destroy/{id}','TehsilController@destroy')->name('tehsil.destroy');

Route::get('farmer_list','HomeController@farmerList')->name('farmerList');
Route::get('farmer_list/{id}','HomeController@findFarmer')->name('findFarmer');
Route::post('farmers/find','HomeController@find')->name('find');
Route::post('farmer_list/searchQuery','HomeController@searchQuery')->name('searchQuery');

// Edit farmer
// Route::get('farmers_list/edit','HomeController@farmersEditList')->name('farmersEditList');
Route::get('farmer_list/edit/{id}','HomeController@farmerEdit')->name('farmerEdit');
Route::put('farmer_list/update/{id}','HomeController@farmerEditSave')->name('farmerEditSave');
// approve section
Route::get('applications','HomeController@applicationlist')->name('applications');
Route::post('applications/approve/{id}','HomeController@approve')->name('approve');
Route::get('applications/find/{id}',"HomeController@viewDetails")->name('viewDetails');
Route::post('applications/resubmit/{id}','HomeController@resubmit')->name('resubmit');
Route::get('resubmitList','HomeController@resubmitList')->name('resubmitList');
Route::get('resubmitList/find/{id}',"HomeController@resubmitViewDetails")->name('resubmitViewDetails');
Route::post('resubmitList/approve/{id}','HomeController@approveResubmit')->name('approveResubmit');
Route::post('resubmitList/resubmit/{id}','HomeController@resubmitResubmit')->name('resubmitResubmit');
Route::delete('resubmitList/delete/{id}','HomeController@destroy_pond_web')->name('destroy_pond_web');
Route::post('applications/sms','HomeController@sendSMS')->name('sendSMS');

// Excel
// List all fishponds
Route::get('/excel/index','ExcelController@index')->name('excel.index');
// Export to excel
Route::post('/excel/exportExcel','ExcelController@exportExcel');
Route::post('/excel/exportExcelAizawl','ExcelController@exportExcelAizawl');
Route::post('/excel/exportExcelKolasib','ExcelController@exportExcelKolasib');
Route::post('/excel/exportExcelLawngtlai','ExcelController@exportExcelLawngtlai');
Route::post('/excel/exportExcelLunglei','ExcelController@exportExcelLunglei');
Route::post('/excel/exportExcelMamit','ExcelController@exportExcelMamit');
Route::post('/excel/exportExcelSiaha','ExcelController@exportExcelSiaha');
Route::post('/excel/exportExcelSerchhip','ExcelController@exportExcelSerchhip');
Route::post('/excel/exportExcelChamphai','ExcelController@exportExcelChamphai');
Route::post('/excel/exportExcelHnahthial','ExcelController@exportExcelHnahthial');
Route::post('/excel/exportExcelSaitual','ExcelController@exportExcelSaitual');
Route::post('/excel/exportExcelKhawzawl','ExcelController@exportExcelKhawzawl');