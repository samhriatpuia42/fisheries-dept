var map;
var myLatlng;
$(document).ready(function(){
  success();
  function success(){
    var latval=23.5345;
    var lngval=93.1830;

    myLatlng=new google.maps.LatLng(latval,lngval);
    createMap(myLatlng);
    searchPonds(latval,lngval);
  }

  function fail(){
    alert("It Fails Position");
  }
    
    // create map
    function createMap(myLatlng){
      map = new google.maps.Map(document.getElementById('map'), {
          center: myLatlng,
          zoom: 12, 
        });
        var marker=new google.maps.Marker({
          position: myLatlng,
          map:map
        })
        
    }

    // this makes red marker for position
    function createMarker(latlng,icn,id,name,address,district,pondImages){
        var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: icn,
        // title:  name
      });
      
      marker.addListener('click', function () {
        
        $.post('http://localhost:8000/api/findPropic/'+id,function(match1){
          // console.log(match);
          document.getElementById('propic').innerHTML = '';
            var x = document.createElement("IMG");
            x.setAttribute("src", "/public/image/"+match1);
            x.setAttribute("width", "80");
            x.setAttribute("height", "80");
            x.setAttribute("style", "display:block;border-radius:50%;margin:9px 9px");
            var id=document.getElementById('propic').appendChild(x);
            // document.getElementById("propic").addEventListener('click',function(){
            //   window.open("/public/image/"+match1);
            // });
      });
      $.post('http://localhost:8000/api/findImage_one/'+id,function(match){
          console.log("image one "+match);
          document.getElementById('pondImage1').innerHTML = '';
          
          // $.each(match,function(i,val){
            var y = document.createElement("IMG");
            
            y.setAttribute("src", "/public/image1/"+match);
            y.setAttribute("width", "100");
            y.setAttribute("height", "100");
            y.setAttribute("style", "display:block;");        
            var id=document.getElementById('pondImage1').appendChild(y);
            
            document.getElementById('ModalpondImage1').innerHTML = '';
            var z = document.createElement("IMG");
            z.setAttribute("src", "/public/image1/"+match);
            z.setAttribute("width", "730");
            z.setAttribute("height", "510");
            z.setAttribute("style", "display:block;");   
            var id2=document.getElementById('ModalpondImage1').appendChild(z);
          // });
      });
      $.post('http://localhost:8000/api/findImage_two/'+id,function(match){
          console.log(match);
          document.getElementById('pondImage2').innerHTML = '';
            var y = document.createElement("IMG");
            y.setAttribute("src", "/public/image2/"+match);
            y.setAttribute("width", "100");
            y.setAttribute("height", "100");
            y.setAttribute("style", "display:block;");        
            var id=document.getElementById('pondImage2').appendChild(y);

            document.getElementById('ModalpondImage2').innerHTML = '';
            var z = document.createElement("IMG");
            z.setAttribute("src", "/public/image2/"+match);
            z.setAttribute("width", "730");
            z.setAttribute("height", "510");
            z.setAttribute("style", "display:block;");   
            var id2=document.getElementById('ModalpondImage2').appendChild(z);
            
      });

      $.post('http://localhost:8000/api/findImage_three/'+id,function(match){
          console.log(match);
          document.getElementById('pondImage3').innerHTML = '';
            var y = document.createElement("IMG");
            y.setAttribute("src", "/public/image3/"+match);
            y.setAttribute("width", "100");
            y.setAttribute("height", "100");
            y.setAttribute("style", "display:block;");        
            var id=document.getElementById('pondImage3').appendChild(y);

            document.getElementById('ModalpondImage3').innerHTML = '';
            var z = document.createElement("IMG");
            z.setAttribute("src", "/public/image3/"+match);
            z.setAttribute("width", "730");
            z.setAttribute("height", "510");
            z.setAttribute("style", "display:block;");   
            var id2=document.getElementById('ModalpondImage3').appendChild(z);
      });

      $.post('http://localhost:8000/api/findImage_four/'+id,function(match){
          console.log(match);
          document.getElementById('pondImage4').innerHTML = '';
            var y = document.createElement("IMG");
            y.setAttribute("src", "/public/image4/"+match);
            y.setAttribute("width", "100");
            y.setAttribute("height", "100");
            y.setAttribute("style", "display:block;");        
            var id=document.getElementById('pondImage4').appendChild(y);

            document.getElementById('ModalpondImage4').innerHTML = '';
            var z = document.createElement("IMG");
            z.setAttribute("src", "/public/image4/"+match);
            z.setAttribute("width", "730");
            z.setAttribute("height", "510");
            z.setAttribute("style", "display:block;");   
            var id2=document.getElementById('ModalpondImage4').appendChild(z);
      });

      $.post('http://localhost:8000/api/findPondWeb/'+id,function(match){
        console.log(match);
            var faddress=document.getElementById('address').innerHTML = match.address;
            var fdistrict=document.getElementById('district').innerHTML = match.district;
            var fname=document.getElementById('name').innerHTML=match.name;
            var fcontact=document.getElementById('contact').innerHTML=match.contact;
            var fepic=document.getElementById('epic_no').innerHTML=match.epic_no;
            var fid=document.getElementById('fname').innerHTML=match.fname;
            var ftehsil=document.getElementById('tehsil').innerHTML=match.tehsil;
            var fschemes=document.getElementById('schemes').innerHTML=match.name_of_scheme;
            var farea=document.getElementById('area').innerHTML=match.area_of_pond;
      });      
      
      document.getElementById("mySidenav").style.width = "350px";
    });

    }
    
    function searchPonds(lat,lng){
      $.post('http://localhost:8000/api/searchPondsKhawzawl',{lat:lat,lng:lng},function(match){
        // console.log(match);
        $.each(match,function(i,val){
            var glatval=val.lat;
            var glngval=val.lng;
            var gid=val.id;
            var gname=val.fname;
            var gcontact=val.contact;
            var gaddress=val.address;
            var gdistrict=val.district;
            var gpondImages=val.pondImage_one;
            var GLatlng=new google.maps.LatLng(glatval,glngval);
            var gicn='https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            createMarker(GLatlng,gicn,gid,gname,gcontact,gaddress,gdistrict,gpondImages);
        });
      });
    }
});
 // AIzaSyBxbv_wIn2sTcqUkUpHwlCE47ak8WIcjmE
/* Close */
function closeNav() {
  document.getElementById("mySidenav").style.width = "0%";
}
 