@extends('layouts.layout')
@section('content')  
<style>
  .is-dangerous{
    border: 2px solid #e74c3c !important;
  }
</style>
<div class="container">
    <div class="justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-4 h-md-100 border-right" style="background: white">
                    <form method="POST" action="/tehsils/create"  style="padding:15px">
                      @csrf
                        <label class="mt-2" style="color:#1473e7;">Add Tehsil</label>
                        <div class="form-group">
                            <input class="form-control{{($errors->first('tname') ? " is-dangerous" : "")}}" type="text" name="tname" placeholder="Enter Tehsil Name" style="border-radius:12px">
                            <p class="help" style="color:red">{{ $errors->first('tname') }}</p>
                        </div>
                        <div class="form-group" >
                          <label class="mt-1">Select Tehsil</label>
                            <select class="form-control" name="district" id="district" style="border-radius:12px">
                              <option value="" disabled selected >Select District</option>
                              <option>Aizawl</option>
                              <option>Hnahthial</option>
                              <option>Kolasib</option>
                              <option>Khawzawl</option>
                              <option>Lawngtlai</option>
                              <option>Lunglei</option>
                              <option>Mamit</option>
                              <option>Siaha</option>
                              <option>Saitual</option>
                              <option>Serchhip</option>
                              <option>Champhai</option>
                             
                            </select>
                        </div>
                        
                        
                        <button type="submit" class="btn btn-primary mt-3 btn-block btn-sm" style="border-radius:12px">ADD</button>
                    </form>
                </div>
                <div class="col-sm mt-3">
                  <div class="mb-2">
                      <div id="pi" class="container" style="color:#1473e7;font-size:20px;font-weight:600">List of Tehsils</div>
                  </div>
                  <table class="table table-striped mt-4 ml-2" >
                      <thead style="background:#1473E7;color:white">
                          <tr>
                              <th scope="col">SI</th>
                              <th scope="col">Tehsil</th>
                              <th scope="col">District</th>
                              <th style="text-align:right">Action</th>
                            
                          </tr>
                      </thead>
                      <tbody>
                          @foreach ($tehsil as $tehsils)
                          <tr>
                              <th scope="row">{{ $tehsils->id }}</th>
                              <td> {{ $tehsils->tname }} </td>
                              <td>{{ $tehsils->district }}</td>
                              <td style="text-align:right"> 
                                  {{-- <a class="btn btn-info">Edit</a> --}}
                                  <form action="{{ route('tehsil.destroy', $tehsils->id)}}" method="POST">
                                      @csrf
                                      @method('delete')
                                      <button type="submit" class="btn btn-outline-light">
                                          <img src="{{ asset('image/delete.png') }}">
                                      </button>
                                  </form>
                                  {{-- <a class="btn btn-info">Delete</a> --}}
                              </td>
                              
                          </tr>
                          @endforeach
                          
                      </tbody>
                  </table>
                  {{ $tehsil->links() }}
                </div>
              
            </div>
        </div>
    </div>
</div>
<div class="chatbox chatbox--tray chatbox--empty">
    <div class="chatbox__title">
        <h5><a href="#">Send SMS</a></h5>
    </div>
    <form class="chatbox__credentials" method="POST" action="applications/sms">
        @csrf
        <div class="form-group">
            <select name="tehsil" class="sms-input-border" required>
                <option value="" disabled selected>Select Tehsil</option>
                @foreach ($tehsil as $tehsils)
                    <option>{{ $tehsils->tname }}</option>
                @endforeach      
            </select>
        </div>
        <br>
        <div class="form-group">
            <textarea class="sms-input-border" rows="1" id="comment" name="message" required></textarea>
        </div>
        <button type="submit" class="btn btn-success btn-block" style="background-color:#007bff"  onclick="popup()">Send</button>
    </form>      
</div>
</div> 
</div>


@endsection
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
function popup() {
alert("Message will be sent!");
}
(function($) {
$(document).ready(function() {
var $chatbox = $('.chatbox'),
    $chatboxTitle = $('.chatbox__title'),
    
    $chatboxCredentials = $('.chatbox__credentials');
$chatboxTitle.on('click', function() {
    $chatbox.toggleClass('chatbox--tray');
});
});
})(jQuery);
</script>

