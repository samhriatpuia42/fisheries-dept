<div class="container" style="background-color: white;">
    <div class="row justify-content-start">
        <div class="col-sm-12 col-md-2">
            <a href="https://india.gov.in" target="_blank"><img src={{ asset('image/india.png') }}></a>
        </div>
        <div class="col-sm-12 col-md-2">
            <a href="https://www.nvsp.in/" target="_blank"><img src={{ asset('image/osv2.png') }}></a>
        </div>
        <div class="col-sm-12 col-md-2">
            <a href="https://deity.gov.in/" target="_blank"><img src={{ asset('image/deity.png') }}></a>
        </div>
        <div class="col-sm-12 col-md-2">
            <a href="https://digitalindia.gov.in/" target="_blank"><img src={{ asset('image/digital.png') }}></a>
        </div>
        <div class="col-sm-12 col-md-2">
            <a href="https://www.mygov.in/" target="_blank"><img src={{ asset('image/mygov.png') }}></a>
        </div>
        <div class="col-sm-12 col-md-2">
            <a href="https://mizoramtenders.gov.in/nicgep/app" target="_blank"><img src={{ asset('image/epro.png') }}></a>
        </div>
    </div>
</div>
<footer>

    <div class="container footer-nav">
        <div class="nav-list">


            <div class="msegs">
                <p>Crafted with care by <a href="https://msegs.mizoram.gov.in/" target="_blank"> Mizoram State e-Governance Society (MSeGS)</a></p>
                <p>Hosted by <a href="https://dict.mizoram.gov.in/" target="_blank"> Department of ICT</a>, Government of Mizoram</p>
            </div>

        </div>

        <div class="container footer-b ">
            <img src={{ asset('images/footerlinelong.jpg') }} class="line-close">
            <!--img src="images/footerlinelong.jpg" class="line-close"-->
            <img style="height:33px; width:33px" src={{ asset('images/chakra2.svg') }}>
            <img src={{ asset('images/footerlinelong.jpg') }} class="line-close">
        </div>
    </div>
</footer>
<br><br>
