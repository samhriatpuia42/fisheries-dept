<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/v4-shims.css">
    
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
</head>
<body>
    <div style="background-color:#1473e7;">
        
        <nav class="navbar navbar-expand-lg navbar-dark container" style="background-color: #1473e7">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" >
                    <a class="navbar-brand">
                        <img src="{{asset('image/logo.png')}}" style="width:190px;height:auto; margin-top: -7px;">
                    </a>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('farmerList') }}">Farmers <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Ponds
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/map/all">All</a>
                            <a class="dropdown-item" href="/map/aizawl">Aizawl</a>
                            <a class="dropdown-item" href="/map/kolasib">Kolasib</a>
                            <a class="dropdown-item" href="/map/lawngtlai">Lawngtlai</a>
                            <a class="dropdown-item" href="/map/lunglei">Lunglei</a>
                            <a class="dropdown-item" href="/map/mamit">Mamit</a>
                            <a class="dropdown-item" href="/map/siaha">Siaha</a>
                            <a class="dropdown-item" href="/map/serchhip">Serchhip</a>
                            <a class="dropdown-item" href="/map/champhai">Champhai</a>
                            <a class="dropdown-item" href="/map/hnahthial">Hnahthial</a>
                            <a class="dropdown-item" href="/map/saitual">Saitual</a>
                            <a class="dropdown-item" href="/map/khawzawl">Khawzawl</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Applications
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('applications') }}">Fresh</a>
                            <a class="dropdown-item" href="{{ route('resubmitList') }}">Resubmit</a>
                            <a class="dropdown-item" href="{{ route('tehsil.index') }}">Tehsils</a>
                            <a class="dropdown-item" href="{{ route('schemes.index') }}">Schemes</a>
                            <a class="dropdown-item" href="{{ route('excel.index') }}">Download</a>
                            {{-- <a class="dropdown-item" href="{{ route('farmersEditList') }}">Edit</a> --}}
                        </div>
                    </li>
                    <div style="border-left: 1px solid white;height: 40px;margin-left:20px;margin-right:20px"></div>
                    @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                {{-- <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> --}}
                            @endif
                        @else
                            <li class="nav-item dropdown" >
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
    
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" >
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                </ul>
            
            </div>
        </nav>
       
    </div> 

    <div class="container">
        @yield('content')
    </div>

    <div id="footer" style="background: white;position: fixed;bottom: 0px;width:100%;border-top: 1px solid grey">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div style="padding-left:18px;display:flex; flex-direction:row;padding-top:7px;padding-bottom:5px">
                        <img src="{{asset('image/Emblem_of_India.png')}}" height="36" width="20">
                        <div style="display:flex; flex-direction:column;flex-wrap:wrap;padding-left:16px">
                            <b><div style="padding-top:10px;font-size:9px">Owned By</div>
                            <div style="line-height:80%;font-size:12px">Department of Fisheries, GoM</div></b>
                        </div>
                    </div>                
                </div>
                <div class="col-sm-5">
                    <div style="margin-left:15px;display:flex; flex-direction:row;padding-top:7px;padding-bottom:5px">
                        <img src="{{asset('image/msegs.png')}}" height="35" width="30" style="padding-top:4px">
                        <div style="display:flex; flex-direction:column;flex-wrap:wrap;padding-left:16px">
                            <b><div style="padding-top:7px;font-size:9px">Developed By</div>
                            <div style="line-height:80%;font-size:12px">Mizoram State e-Governance Society</div></b>
                        </div>
                    </div>  
                </div>
            </div>
          </div>
    </div>

    
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>