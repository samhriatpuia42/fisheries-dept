@extends('layouts.layout')
    
@section('content')
<div class="container">
   <div class="row">
      <div class="col-sm-6">
         <div class="container mt-2 mb-2" style="color:#1473e7;font-size:20px;font-weight:600">Download Excel</div>
         <table class="table table-hover ml-3" style="color: white">
            <thead class="table" style="background: #1473e7">
               <tr>
                  <th scope="col">District name</th>
                  <th style="text-align:center">Download</th>
               </tr>
            </thead>
            <tbody style="color:#949494">
               <tr>
                  <th scope="row">All District</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcel'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexcel" value='Download' class="btn btn-sm btn-primary">
                     </form>      
                  </td>
               </tr>
               <tr>
                  <th scope="row">Aizawl</th>
                  <td style="text-align:center">
                        <form method='post' action='/excel/exportExcelAizawl'>
                              {{ csrf_field() }}
                              <input type="submit" name="exportexceAizawl" value='Download' class="btn btn-sm btn-primary">
                        </form>     
                  </td>
               </tr>

               <tr>
                  <th scope="row">Kolasib</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelKolasib'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceKolasib" value='Download' class="btn btn-sm btn-primary">
                     </form>  
                  </td>
               </tr>
               <tr>
                  <th scope="row">Lawngtlai</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelLawngtlai'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceLawngtlai" value='Download' class="btn btn-sm btn-primary">
                     </form> 
                  </td>
               </tr>
               <tr>
                  <th scope="row">Mamit</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelMamit'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceMamit" value='Download' class="btn btn-sm btn-primary">
                     </form>
               </tr>
               <tr>
                  <th scope="row">Siaha</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelSiaha'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceSiaha" value='Download' class="btn btn-sm btn-primary">
                     </form>
                  </td>
               </tr>
               <tr>
                  <th scope="row">Serchhip</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelSerchhip'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceSerchhip" value='Download' class="btn btn-sm btn-primary">
                     </form> 
                  </td>
               </tr>
               <tr>
                  <th scope="row">Champhai</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelChamphai'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceChamphai" value='Download' class="btn btn-sm btn-primary">
                     </form>  
                  </td>
               </tr>
               <tr>
                  <th scope="row">Hnahthial</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelHnahthial'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceHnahthial" value='Download' class="btn btn-sm btn-primary">
                     </form>
                  </td>
               </tr>
               <tr>
                  <th scope="row">Saitual</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelSaitual'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceSaitual" value='Download' class="btn btn-sm btn-primary">
                     </form>
         
                  </td>
               </tr>
               <tr>
                  <th scope="row">Khawzawl</th>
                  <td style="text-align:center">
                  <form method='post' action='/excel/exportExcelKhawzawl'>
                        {{ csrf_field() }}
                        <input type="submit" name="exportexceKhawzawl" value='Download' class="btn btn-sm btn-primary">
                     </form> 
                  </td>
               </tr>
                  
            </tbody>
         </table>

      </div>
      <div class="col-sm">
         
      </div>
   </div>
</div>

    
    

      
      
      

      

     

      

      

      


      


      

      

@endsection
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>