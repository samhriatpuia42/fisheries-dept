@extends('layouts.layout')
    
@section('content')
<div class="d-md-flex h-md-100 align-items-center container">

    <!-- First Half -->
    
    <div class="col-md-3 p-0 bg-white h-md-100 border-right">
        <div class="text-white d-flex flex-column justify-content-left pl-4">
            <div class="mt-3 space-around-div" style="background: #c4deff">
                <div style="color: #136bd5;" class="sizing-text">{{ $users }}</div>
                <div style="color: #136bd5"><small>No. of farmers</small></div>
            </div>

            <div class="mt-2 space-around-div" style="background: #d2c2ff">
                <div style="color: #136bd5" class="sizing-text">{{ $fishponds }}</div>
                <div style="color: #136bd5"><small>No. of approved ponds</small></div>
            </div>

            <div class="mt-2 space-around-div" style="background: #ffdee9">
                <div style="color: #e9266b" class="sizing-text">{{ $applied }}</div>
                <div style="color: #e9266b" ><small>No. of fishponds applied</small></div>
            </div>
        </div>
    </div>
    <!-- Second Half -->
    <div class="col-md-9 p-0 bg-white h-md-100 loginarea">
        <div class=" h-md-100 " style="background: white"><br>
            <div id="pi" class="container" style="color:#1473e7;font-size:20px;font-weight:600">Fresh Applications</div>
            <div class="container mt-3">
                <table class="table table-hover" style="color: white">
                    <thead class="table" style="background: #1473e7">
                        <tr>
                            <th scope="col">SI</th>
                            <th scope="col">Name</th>
                            <th scope="col" style="width: 40%;">Address</th>
                            <th scope="col">District</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody style="color:#949494">
                        @foreach ($applications as $application)
                        <tr>
                            <th scope="row">{{ $application->id }}</th>
                            <td>{{ $application->name }}</td>
                            <td>{{ $application->address }}</td>
                            <td>{{ $application->district }}</td>
                            <td style="text-align:center">
                                <a href="/applications/find/{{ $application->id }}"    
                                    name="title">
                                    <img src="{{ asset('image/eye.png') }}" height="15" width="20" class="rounded border">  
                                </a>         
                            </td>
                        </tr>
                            @endforeach
                    </tbody>
                </table>
                {{ $applications->links() }}
            </div>
        </div>
        <div class="chatbox chatbox--tray chatbox--empty">
            <div class="chatbox__title">
                <h5><a href="#">Send SMS</a></h5>
            </div>
            <form class="chatbox__credentials" method="POST" action="applications/sms">
                @csrf
                <div class="form-group">
                    <select name="tehsil" class="sms-input-border" required>
                        <option value="" disabled selected>Select Tehsil</option>
                        @foreach ($tehsils as $tehsil)
                            <option>{{ $tehsil->tname }}</option>
                        @endforeach      
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <textarea class="sms-input-border" rows="1" id="comment" name="message" required></textarea>
                </div>
                <button type="submit" class="btn btn-success btn-block" style="background-color:#007bff"  onclick="popup()">Send</button>
            </form>      
        </div>
    </div> 
</div>


@endsection
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    function popup() {
        alert("Message will be sent!");
    }
    (function($) {
    $(document).ready(function() {
        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxCredentials = $('.chatbox__credentials');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });  
    });
})(jQuery);
    </script>