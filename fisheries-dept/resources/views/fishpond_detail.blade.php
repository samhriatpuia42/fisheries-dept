@extends('layouts.layout')
    
@section('content')
<div class="d-md-flex h-md-100 align-items-center container">

    <!-- First Half -->
    
    <div class="col-md-3 p-0 bg-white h-md-100 ">
        <div class="text-white d-flex flex-column justify-content-left pl-4">
            <div class="mt-3 space-around-div" style="background: #c4deff">
                <div style="color: #136bd5;" class="sizing-text">{{ $users }}</div>
                <div style="color: #136bd5"><small>No. of farmers</small></div>
            </div>

            <div class="mt-2 space-around-div" style="background: #d2c2ff">
                <div style="color: #136bd5" class="sizing-text">{{ $fishponds }}</div>
                <div style="color: #136bd5"><small>No. of approved ponds</small></div>
            </div>

            <div class="mt-2 space-around-div" style="background: #ffdee9">
                <div style="color: #e9266b" class="sizing-text">{{ $applied }}</div>
                <div style="color: #e9266b" ><small>No. of fishponds applied</small></div>
            </div>
        </div>
    </div>
    
    <!-- Second Half -->
    <div class="col-md-9 p-0 bg-white h-md-100 loginarea mt-5">
        <div class="card " style="width: 51rem;box-shadow: 0 0 1px;">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="container mt-2 " >
                        <div class="row ml-4">
                            <div class="col-sm">
                                <div style="display: flex;justify-content:space-between">
                                    <div style="display:flex;">
                                        <img src="{{ asset('/public/image/'.$fishpond->image) }}" class="rounded-circle border" width="80" height="80">
                                        <div class="mt-3" style="padding-left:10px">
                                            <div>
                                                {{ $fishpond->name }}
                                            </div> 
                                            <div>
                                                {{ $fishpond->address }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="p-2">
                                    <a href="{{ route('applications') }}" class="float-right" style="color:#aeaeae"><u>Back</u></a>
                                </div><br>
                                <form action="/applications/approve/{{ $fishpond->id }}" method="POST" class="p-2 float-right">
                                    @csrf
                                    <button type="submit" class="btn btn-primary  rounded btn-sm">APPROVE</button>
                                </form>
                                
                                {{-- <form action="/applications/resubmit/{{ $fishpond->id }}" method="POST" class="p-2">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-primary rounded  btn-sm float-right">RESUBMIT</button>
                                </form> --}}
                            </div>
                            
                        </div>
                    </div>
                </li>
                <li class="list-group-item">     
                    <div class="container mt-4 mb-5" >
                        <div class="row ml-4">
                            <div class="col-sm">
                                <div class="container " >
                                    <div class="row">
                                        <div class="col-sm-3">
                                            Father's name <br>
                                            Epic/Adhaar <br>
                                            Contact <br>
                                            District <br>
                                            Tehsil <br>
                                            Lat/Lng <br>
                                            Area <br>
                                            Schemes
                                        </div>
                                        <div class="col-sm-4">
                                            : {{ $fishpond->fname }} <br>
                                            : {{ $fishpond->epic_no }}<br>
                                            : {{ $fishpond->contact }}<br>
                                            : {{ $fishpond->district }} <br>
                                            : {{ $fishpond->tehsil }}<br>
                                            : {{ $fishpond->lat }} / {{ $fishpond->lng }}<br>
                                            : {{ $fishpond->area }}<br>
                                            : {{ $fishpond->name_of_scheme }}
                                        </div>
                                        <div class="col-sm">
                                            <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModalCenter">
                                                <img src="{{ asset('public/image1/'.$fishpond->pondImage_one) }}"  height="90" width="100" class="rounded border">
                                            </button>
                                            
                                            <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModalCenter2">
                                                <img src="{{ asset('public/image2/'.$fishpond->pondImage_two) }}"  height="90" width="100" class="rounded border">
                                            </button>
                                            <br>
                                            <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModalCenter3">
                                                <img src="{{ asset('public/image3/'.$fishpond->pondImage_three) }}"  height="90" width="100" class="rounded border">
                                            </button>

                                            <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModalCenter4">
                                                <img src="{{ asset('public/image4/'.$fishpond->pondImage_four) }}"  height="90" width="100" class="rounded border">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <form action="/applications/resubmit/{{ $fishpond->id }}" method="POST" class="p-2">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Resubmit Message</label>
                                <textarea class="form-control{{($errors->first('reason') ? " is-dangerous" : "")}}" id="exampleFormControlTextarea1" rows="3" name="reason"></textarea>
                                <p class="help" style="color:red">{{ $errors->first('reason') }}</p>
                            </div>
                            <button type="submit" class="btn btn-outline-primary rounded  btn-sm float-right">RESUBMIT</button>
                        </form>
                        
                    </li>
                </ul>
            </div>

        </div>


        <div class="chatbox chatbox--tray chatbox--empty">
            <div class="chatbox__title">
                <h5><a href="#">Send SMS</a></h5>     
            </div>
            <form class="chatbox__credentials" method="POST" action="applications/sms">
                @csrf
                <div class="form-group">
                    <select name="tehsil" class="sms-input-border">
                        <option value="" disabled selected>Select Tehsil</option>
                        @foreach ($tehsils as $tehsil)
                            <option>{{ $tehsil->tname }}</option>
                        @endforeach
                        
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <textarea class="sms-input-border" rows="1" id="comment" name="message"></textarea>
                </div>
                <button type="submit" class="btn btn-success btn-block" style="background-color:#007bff"  onclick="popup()">Send</button>
            </form>      
        </div>    
        </div>
    </div>  
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">First Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('public/image1/'.$fishpond->pondImage_one) }}"  height="600" width="770" class="rounded border">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Second Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('public/image2/'.$fishpond->pondImage_two) }}"  height="600" width="770" class="rounded border">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Third Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('public/image3/'.$fishpond->pondImage_three) }}"  height="600" width="770" class="rounded border">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Fourth Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('public/image4/'.$fishpond->pondImage_four) }}"  height="600" width="770" class="rounded border">
            </div>
        </div>
    </div>
</div>
@endsection
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    function popup() {
        alert("Message will be sent!");
    }
    (function($) {
    $(document).ready(function() {
        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxCredentials = $('.chatbox__credentials');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });
    });
})(jQuery);
    </script>
    