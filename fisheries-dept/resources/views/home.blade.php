@extends('layouts.layout')
    {{-- @extends('layouts.navbar')
    @section('content')
     --}}
@section('content')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <div class="pl-5">
        <div class="container">
            <div class="row container" style="height: 570px;margin: auto;position: absolute;top: 0; left: 0; bottom: 0; right: 0;">
                <div class="col-sm-5">
                    <div style="margin-top:40%">
                        <div style="padding-left:5px;">WELCOME TO</div>
                        <div style="color:#1473e7;font-size:75px;font-weight: 750;line-height: 80%;" class="mt-2">FISH-INFO</div>
                        <div style="color:#1473e7;font-size:72px;font-weight: 750;line-height: 80%;">MIZORAM</div>
                        <br>
                        <a type="button" class="btn btn-outline-primary" href="{{ route('farmerList') }}">VIEW FARMERS</a>
                    </div>
                    
                </div>
                <div class="col-sm-7">
                    <img class="img-fluid" src="{{asset('image/pond2.png')}}" style="width:600px;height:400px;margin-top:10%">
                </div>
            </div>
        </div>
    </div>
@endsection
