@extends('layouts.siahaMapLayout')

@section('content')

    <div style="background-color:#1473e7;">
        <nav class="navbar navbar-expand-lg navbar-dark container" style="background-color: #1473e7">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" >
                    <a class="navbar-brand">
                        <img src="{{asset('image/logo.png')}}" style="width:190px;height:auto; margin-top: -7px;">
                    </a>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('farmerList') }}">Farmers <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Ponds
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/map/all">All</a>
                            <a class="dropdown-item" href="/map/aizawl">Aizawl</a>
                            <a class="dropdown-item" href="/map/kolasib">Kolasib</a>
                            <a class="dropdown-item" href="/map/lawngtlai">Lawngtlai</a>
                            <a class="dropdown-item" href="/map/lunglei">Lunglei</a>
                            <a class="dropdown-item" href="/map/mamit">Mamit</a>
                            <a class="dropdown-item" href="/map/siaha">Siaha</a>
                            <a class="dropdown-item" href="/map/serchhip">Serchhip</a>
                            <a class="dropdown-item" href="/map/champhai">Champhai</a>
                            <a class="dropdown-item" href="/map/hnahthial">Hnahthial</a>
                            <a class="dropdown-item" href="/map/saitual">Saitual</a>
                            <a class="dropdown-item" href="/map/khawzawl">Khawzawl</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Applications
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('applications') }}">Fresh</a>
                            <a class="dropdown-item" href="{{ route('resubmitList') }}">Resubmit</a>
                            <a class="dropdown-item" href="{{ route('tehsil.index') }}">Tehsils</a>
                            <a class="dropdown-item" href="{{ route('schemes.index') }}">Schemes</a>
                            {{-- <a class="dropdown-item" href="{{ route('farmersEditList') }}">Edit</a> --}}
                        </div>
                    </li>
                    <div style="border-left: 1px solid white;height: 40px;margin-left:20px;margin-right:20px"></div>
                    @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                {{-- <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> --}}
                            @endif
                        @else
                            <li class="nav-item dropdown" >
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
    
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" >
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                </ul>
            
            </div>
        </nav>
    </div> 


    <div>
        <div class="sidenav" id="mySidenav" >
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <div class="">
                <div class="container" >
                    <div class="row left-space">
                        <div class="col-4">
                            <div id="propic" style="border-radius: 50%;"></div>
                        </div>
                        <div class="col-8" style="padding-top:9px">
                            <b><div id="name"></div></b>
                            <div id="address"></div>
                            <div id="district"></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="mainframe left-space sidenavmargindetails mt-3">
                <div class="leftframe" style="color: #a4a4a4">Contact</div>
                <div class="rightframe" id="contact"></div>
            </div>
            <div class="mainframe left-space sidenavmargindetails">
                <div class="leftframe" style="color: #a4a4a4">Father's Name:</div>
                <div class="rightframe" id="fname"></div>
            </div>
          
            <div class="mainframe left-space sidenavmargindetails">
                <div class="leftframe" style="color: #a4a4a4">EPIC:</div>
                <div class="rightframe" id="epic_no"></div>
            </div>

            <div class="mainframe left-space sidenavmargindetails">
                <div class="leftframe" style="color: #a4a4a4">Tehsil</div>
                <div class="rightframe" id="tehsil"></div>
            </div>
            <div class="mainframe left-space sidenavmargindetails">
                <div class="leftframe" style="color: #a4a4a4">Area</div>
                <div class="rightframe" id="area"></div>
            </div>
            <div class="mainframe left-space sidenavmargindetails">
                <div class="leftframe" style="color: #a4a4a4">Schemes</div>
                <div class="rightframe" id="schemes"></div>
            </div>
            <br>
            <div class="mainframe left-space">
                {{-- <div class="sidenavmargindetails mb-3" style="color: #a4a4a4"> Pond Images:</div> --}}
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-outline-light border" data-toggle="modal" data-target="#exampleModalCenter1">
                                <div id="pondImage1" style="width: 100px"></div>
                            </button>
                        </div>
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-light border" data-toggle="modal" data-target="#exampleModalCenter2">
                                <div id="pondImage2" style="width: 100px"></div>
                            </button>
                        </div>
                    </div>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-outline-light border" data-toggle="modal" data-target="#exampleModalCenter3">
                                <div id="pondImage3"></div>
                            </button>
                        </div>
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-light border" data-toggle="modal" data-target="#exampleModalCenter4">
                                <div id="pondImage4"></div>
                            </button>
                        </div>
                    </div>
                  </div>
            </div>    
        </div>

        <div class="container-fluid map-width" style="width:80%;">
            <div id="map">

            </div>
        </div>
        <div id="footer" style="background: white;position: fixed;bottom: 0px;width:100%;border-top: 1px solid grey">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <div style="padding-left:18px;display:flex; flex-direction:row;padding-top:7px;padding-bottom:5px">
                            <img src="{{asset('image/Emblem_of_India.png')}}" height="36" width="20">
                            <div style="display:flex; flex-direction:column;flex-wrap:wrap;padding-left:16px">
                                <b><div style="padding-top:10px;font-size:9px">Owned By</div>
                                <div style="line-height:80%;font-size:12px">Department of Fisheries, GoM</div></b>
                            </div>
                        </div>                
                    </div>
                    <div class="col-sm-5">
                        <div style="margin-left:15px;display:flex; flex-direction:row;padding-top:7px;padding-bottom:5px">
                            <img src="{{asset('image/msegs.png')}}" height="35" width="30" style="padding-top:4px">
                            <div style="display:flex; flex-direction:column;flex-wrap:wrap;padding-left:16px">
                                <b><div style="padding-top:7px;font-size:9px">Developed By</div>
                                <div style="line-height:80%;font-size:12px">Mizoram State e-Governance Society</div></b>
                            </div>
                        </div>  
                    </div>
                </div>
              </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">First Pond Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="ModalpondImage1" style="width: 800px"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Second Pond Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="ModalpondImage2" style="width: 800px"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Third Pond Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="ModalpondImage3" style="width: 800px"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Fourth Pond Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="ModalpondImage4" style="width: 800px"></div>
            </div>
        </div>
    </div>
</div>

