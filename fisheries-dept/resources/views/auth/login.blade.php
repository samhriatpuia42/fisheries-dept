@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="container">
            <div class="row container" style="height: 570px;margin: auto;position: absolute;top: 0; left: 0; bottom: 0; right: 0;">
                <div class="col-md-7">
                    <img class="img-fluid" src="{{asset('image/pond.png')}}" style="width:600px;height:520px;">
                </div>
                <div class="col-sm-5">
                    <div class="card" style="margin-top: 30px;width: 280px;">                       
                        <div class="card-body">
                            <h5 class="card-title text-center" style="font-size:30px;color: #1473e7;margin-bottom:50px;">Login</h5>
                            <form method="POST" action="{{ route('login') }}" >
                                @csrf
                                <div class="form-group">
                                    <input class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Username" autofocus style="width: 230px;border-radius: 10px;">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">                                 
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" style="width: 230px;border-radius: 10px;">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} >
                                        <label class="form-check-label" for="remember" style="color:#bbbbbb">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div> 
                                </div>
                               <br>
                                <button type="submit" class="btn btn-primary btn-block" style="width: 230px;border-radius: 10px;">
                                    {{ __('LOGIN') }}
                                </button>
                                
                                @if (Route::has('password.request'))
                                <div style="text-align:center;padding-right:10px">
                                    <a class="" href="{{ route('password.request') }}">
                                        <u>{{ __('Forgot Password?') }}</u>
                                    </a>
                                </div>
                                    <br><br>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection
