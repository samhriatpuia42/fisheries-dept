@extends('layouts.layout')
@section('content')  
<style>
  .is-dangerous{
    border: 2px solid #e74c3c !important;
  }
</style>
<div class="container">
    <div class="justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-4 h-md-100 border-right" style="background: white">
                  <form method="POST" action="/schemes/create">
                    @csrf
                    <label class="mt-4 ml-1" style="color:#1473E7;">Create New Scheme</label>
                    <div class="form-group">
                      <input  
                        class="form-control{{($errors->first('sname') ? 'is-dangerous' : '')}}" type="text" name="sname" placeholder="Enter Scheme Name" style="border-radius:12px">
                      <p class="help" style="color:red">{{ $errors->first('sname') }}</p>
                    </div> 
                    <button type="submit" class="btn btn-primary btn-small mt-3  btn-block" style="border-radius:12px">Add</button>
                  </form>
                </div>
                <div class="col-sm mt-3">
                  <div class="mb-2">
                      <div id="pi" class="container" style="color:#1473e7;font-size:20px;font-weight:600">List of Schemes</div>
                  </div>
                  <table class="table table-striped">
                    <thead style="background:#1473E7;color:white">
                      <tr>
                        <th scope="col">SI</th>
                        <th scope="col">Scheme Name</th>
                        <th scope="col" style="text-align:right">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($scheme as $schemes)
                        <tr>
                            <th scope="row">{{ $schemes->id }}</th>
                            <td> {{ $schemes->sname }} </td>
                            <td style="text-align:right"> 
                                <form action="{{ route('schemes.delete', $schemes->id)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-outline-light">
                                        <img src="{{ asset('image/delete.png') }}">
                                    </button>
                                </form>
                            </td>
                            
                        </tr>
                        @endforeach
                        
                    </tbody>
                  </table>
                  {{ $scheme->links() }}
                </div>
              
            </div>
        </div>
    </div>
</div>
<div class="chatbox chatbox--tray chatbox--empty">
    <div class="chatbox__title">
        <h5><a href="#">Send SMS</a></h5>
    </div>
    <form class="chatbox__credentials" method="POST" action="applications/sms">
        @csrf
        <div class="form-group">
            <select name="tehsil" class="sms-input-border" required>
                <option value="" disabled selected>Select Tehsil</option>
                @foreach ($tehsil as $tehsils)
                    <option>{{ $tehsils->tname }}</option>
                @endforeach      
            </select>
        </div>
        <br>
        <div class="form-group">
            <textarea class="sms-input-border" rows="1" id="comment" name="message" required></textarea>
        </div>
        <button type="submit" class="btn btn-success btn-block" style="background-color:#007bff"  onclick="popup()">Send</button>
    </form>      
</div>
</div> 
</div>


@endsection
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
function popup() {
alert("Message will be sent!");
}
(function($) {
$(document).ready(function() {
var $chatbox = $('.chatbox'),
    $chatboxTitle = $('.chatbox__title'),
    
    $chatboxCredentials = $('.chatbox__credentials');
$chatboxTitle.on('click', function() {
    $chatbox.toggleClass('chatbox--tray');
});
});
})(jQuery);
</script>


