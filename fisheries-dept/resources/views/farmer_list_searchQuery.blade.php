@extends('layouts.layout')
    
@section('content')

<div class="d-md-flex h-md-100 align-items-center container">
    <!-- First Half -->
    <div class="col-md-4 p-0 bg-white h-md-100 border-right">
        <div class="d-flex flex-column justify-content-left pl-4">
            <form class="form-inline mt-1" method="POST" action="farmers/find" >
                @csrf
                <div style="border: 1px solid #9c9c9c; border-radius: 0.3rem;" class="ml-3 mt-3">
                    <input style="border: none;width:245px;" type="text" class="form-control" name="contact" placeholder="Search Phone No." required/>
                    <button class="btn btn-link" id="basic-addon2"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <div class="mt-3 " style="margin-right:28px">
                @foreach ($farmers as $farmer)
                    <div class="farmer-hover">
                        <div class="container" style="font-size: small">
                            <div class="row farmer-border" onclick="searchFarmer({{ $farmer->id }})" style="padding-bottom:5px;padding-top:5px">
                                <div class="col-sm-4">
                                    <img src="/public/image/{{ $farmer->image }}" class="profile-pic">
                                </div>
                                
                                <div class="col-sm-6 ">
                                    {{ $farmer->name }} <br>
                                    {{ $farmer->location_of_pond }}
                                    <br style="margin-bottom:5px">
                                    <div class="row">
                                        <div class="col-md-4">{{ $farmer->district }}</div>
                                    </div>
                                </div> 
                                
                                <div class="col-sm-2">
                                    <a href="/farmer_list/edit/{{ $farmer->id }}"    
                                        name="title" data-placement="bottom" title="Edit details">
                                        <i class="fas fa-pencil-alt mt-4"></i>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                @endforeach
                {{ $farmers->links() }}
            </div>
        </div>
        
    </div>
    
    <!-- Second Half -->
    <div class="col-8 p-0 h-md-100 loginarea" style="width: 400px">
        <div class="container mt-4 ml-5">
            <form method="POST" action="/farmer_list/searchQuery">
                @csrf
                <div class="row">
                    <div class="col-sm-5">
                        <select class="form-control form-control-sm" name="district">
                            <option value="" disabled selected >Select District</option>
                            <option>Aizawl</option>
                            <option>Hnahthial</option>
                            <option>Kolasib</option>
                            <option>Khawzawl</option>
                            <option>Lawngtlai</option>
                            <option>Lunglei</option>
                            <option>Mamit</option>
                            <option>Siaha</option>
                            <option>Saitual</option>
                            <option>Serchhip</option>
                            <option>Champhai</option>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control form-control-sm" style="color:black" name="sname">
                            <option value="" disabled selected >Select Scheme</option>
                            @foreach ($schemes as $scheme)
                                <option>{{ $scheme->sname }}</option>
                            @endforeach
                            
                           
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="h-md-100">
            <div id="" >
                <div class="col ml-3" id="image_one"></div>
                <div class="ml-5 pt-3" id="background" style="width: 424px;">
                    <div class="container" >
                        <div class="row ml-3" >
                            <div class="col-1.5" style="text-align: right">
                                <div id="pf"></div>
                            </div>
                            <div class="col-6 ml-4" >
                                <b><div id="name" style="margin-left: 10px" class="mt-3"></div></b>
                                <small>
                                <div id="address" style="margin-left: 10px"></div>
                                <div id="district" style="margin-left: 10px"></div>
                                </small>
                            </div>
                        </div>
                        <br>
                        <div class="row ml-3" >
                            <div class="col-4">
                                <div id="fnamet" class="text-color-farmerlist"></div>
                                <div id="epict" class="text-color-farmerlist"></div>
                                <div id="contactt" class="text-color-farmerlist"></div>
                                
                                <div id="tehsilt" class="text-color-farmerlist"></div>
                                <div id="areat" class="text-color-farmerlist"></div>
                                <div id="schemet" class="text-color-farmerlist"></div>
                            </div>
                            <div class="col-6">
                                <div id="fname"></div>
                                <div id="epic"></div>
                                <div id="contact"></div>
                                <div id="tehsil"></div>
                                <div id="area"></div>
                                <div id="scheme"></div>
                                <div id="button"></div>
                            </div>
                        </div>
                    </div>
                </div>
                 
            </div>
             
        </div>
    </div> 
</div>

@endsection
<script crossorigin="anonymous" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" src="https://code.jquery.com/jquery-3.1.0.min.js">
</script>




<script>
    function searchFarmer(id) {
        $.get('http://localhost:8000/farmer_list/'+id,function(data){
        console.log(data.id);
        arg=data.id;
        
        var tehsil=document.getElementById('fnamet').innerHTML="Father's Name";
        var id=document.getElementById('epict').innerHTML="Epic/Adhaar";
        var contact=document.getElementById('contactt').innerHTML="Contact";
        var schemes=document.getElementById('schemet').innerHTML="Schemes";
        var tehsil=document.getElementById('tehsilt').innerHTML="Tehsil";
        var area=document.getElementById('areat').innerHTML="Area";

        // pondImage one
        var pondimage_one=document.getElementById('image_one').innerHTML = '';
            var y=document.createElement("IMG");
            y.setAttribute("src", "/public/image1/"+data.image);
            y.setAttribute("width", "424");
            y.setAttribute("height", "150");
            y.setAttribute("style", "margin-left:17px");
            y.setAttribute("class", "border-bottom mt-2");
            var id=document.getElementById('image_one').appendChild(y);
        // profile image
        
            var pff=document.getElementById('pf').innerHTML = '';
            var y=document.createElement("IMG");
            y.setAttribute("src", "/public/image/"+data.image);
            y.setAttribute("width", "90");
            y.setAttribute("height", "90");
            y.setAttribute("style", "display:block;margin-left:10px");
            y.setAttribute("class", "rounded-circle");

            var background=document.getElementById('background').style.backgroundColor = "white";
            var idd=document.getElementById('pf').appendChild(y);

            var name=document.getElementById('name').innerHTML=data.name;
            var idd=document.getElementById('address').innerHTML = data.address;
            var idd=document.getElementById('district').innerHTML = data.district;
           
            var tehsil=document.getElementById('fname').innerHTML=": "+data.fname;
            var idd=document.getElementById('epic').innerHTML=": "+data.epic_no;
            var tehsil=document.getElementById('contact').innerHTML=": "+data.contact;
            var tehsil=document.getElementById('tehsil').innerHTML=": "+data.tehsil;
            var tehsil=document.getElementById('area').innerHTML=": "+data.area+" Ha";
            

            list = data.name_of_scheme.split(",");
            document.getElementById("scheme").innerHTML="";
           
            for(var i=0; i<list.length; i++)
            {
                
                var x = document.createElement("LI");
                var t = document.createTextNode(list[i]);
                x.setAttribute("style", "list-style-type:none");
                x.appendChild(t);
                
                document.getElementById("scheme").appendChild(x);

            }
            
      });
      
      
    }
</script>