@extends('layouts.layout')
    
@section('content')
<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col-sm">
          <div class="ml-3" style="line-height: 80%;font-size:26px;color:#007bff;font-weight:bold">Information</div>
          <div class="ml-3" style="font-size:15px;color:#b1b1b1">Please! Enter correct inforamtion</div>
            <form method="POST" action="/farmer_list/update/{{ $farmer->id }}" class="mt-4">
                @csrf
                @method('PUT')
                <div class="form-group ">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                              <label for="exampleFormControlInput1">Name</label>
                            </div>
                            <div class="col-sm">
                              <input type="text" name="name" class="form-control form-control-sm {{($errors->first('name') ? " is-dangerous" : "")}}" id="exampleFormControlInput1" value="{{ $farmer->name }}">
                              <p class="help" style="color:red">{{ $errors->first('name') }}</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id"class="form-control form-control-sm {{($errors->first('id') ? " is-dangerous" : "")}}" id="exampleFormControlInput1" value="{{ $farmer->id }}">
                    <input type="hidden" name="user_id" class="form-control form-control-sm {{($errors->first('user_id') ? " is-dangerous" : "")}}"  value="{{ $farmer->user_id }}">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                              <label for="exampleFormControlInput1">Contact</label>
                            </div>
                            <div class="col-sm">
                              <input type="text" name="contact" class="form-control form-control-sm {{($errors->first('contact') ? " is-dangerous" : "")}}" id="exampleFormControlInput1" value="{{ $farmer->contact }}">
                              <p class="help" style="color:red">{{ $errors->first('contact') }}</p>
                            </div>
                        </div>
                    </div>
                
                  
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="exampleFormControlInput1">Address</label>
                            </div>
                            <div class="col-sm">
                                <input type="text" name="address" class="form-control form-control-sm {{($errors->first('address') ? " is-dangerous" : "")}}" id="exampleFormControlInput1" value="{{ $farmer->address }}">
                                <p class="help" style="color:red">{{ $errors->first('address') }}</p>
                            </div>
                        </div>
                    </div>   
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="exampleFormControlInput1">District</label>
                            </div>
                            <div class="col-sm">
                                <input type="text" name="district" class="form-control form-control-sm {{($errors->first('district') ? " is-dangerous" : "")}}" id="exampleFormControlInput1" value="{{ $farmer->district }}">
                                <p class="help" style="color:red">{{ $errors->first('district') }}</p>
                            </div>
                        </div>
                    </div>

                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                              <label for="exampleFormControlInput1">Name of Scheme</label>
                          </div>
                          <div class="col-sm">
                              <input type="text" name="name_of_scheme" class="form-control form-control-sm {{($errors->first('name_of_scheme') ? " is-dangerous" : "")}} id="exampleFormControlInput1" value="{{ $farmer->name_of_scheme }}">
                              <p class="help" style="color:red">{{ $errors->first('name_of_scheme') }}</p>
                          </div>
                      </div>
                  </div>
                  
                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Father's Name</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="fname" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}"  value="{{ $farmer->fname }}">
                            <p class="help" style="color:red">{{ $errors->first('fname') }}</p>
                          </div>
                      </div>
                  </div>
                  
                    <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Epic</label>
                          </div>
                          <div class="col-sm">
                              <input type="text" name="epic_no" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}"  value="{{ $farmer->epic_no }}">
                              <p class="help" style="color:red">{{ $errors->first('epic_no') }}</p>
                          </div>
                      </div>
                    </div>

                    <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Tehsil</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="tehsil" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}" value="{{ $farmer->tehsil }}">
                            <p class="help" style="color:red">{{ $errors->first('tehsil') }}</p>
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Location of pond</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="location_of_pond" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}" value="{{ $farmer->location_of_pond }}">
                            <p class="help" style="color:red">{{ $errors->first('location_of_pond') }}</p>
                          </div>
                      </div>
                  </div>

                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Area of Pond</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="area" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}" value="{{ $farmer->area }}">
                            <p class="help" style="color:red">{{ $errors->first('area') }}</p>
                          </div>
                      </div>
                  </div>

                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Latitude</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="lat" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}" value="{{ $farmer->lat }}">
                            <p class="help" style="color:red">{{ $errors->first('lat') }}</p>
                          </div>
                      </div>
                  </div>


                  <div class="container">
                      <div class="row">
                          <div class="col-sm-4">
                            <label for="exampleFormControlInput1">Longitude</label>
                          </div>
                          <div class="col-sm">
                            <input type="text" name="lng" class="form-control form-control-sm {{($errors->first('fname') ? " is-dangerous" : "")}}" value="{{ $farmer->lng }}">
                            <p class="help" style="color:red">{{ $errors->first('lng') }}</p>
                          </div>
                      </div>
                  </div>
                </div>     
        </div>
        <div class="col-sm mt-3">
            <div class="form-group">
              {{-- <label for="exampleFormControlInput1" style="line-height: 80%;font-size:24px;color:#007bff;font-weight:bold">Profile Image</label><br> --}}
              <img src="{{ asset('public/image/'.$farmer->image) }}" alt="..." height="100" width="150" class="rounded">
            </div>
            <div class="form-group">
              <br>
                <label for="exampleFormControlInput1" style="line-height: 80%;font-size:22px;color:#007bff;font-weight:bold">Pond Images</label><br>
                <img src="{{ asset('public/image1/'.$farmer->pondImage_one) }}"  height="140" width="240" class="rounded border">
                
                <img src="{{ asset('public/image2/'.$farmer->pondImage_two) }}"  height="140" width="240" class="rounded border">
                <br>
                <img src="{{ asset('public/image3/'.$farmer->pondImage_three) }}"  height="140" width="240" class="rounded border">
                <img src="{{ asset('public/image4/'.$farmer->pondImage_four) }}"  height="140" width="240" class="rounded border">
            </div>
            
        </div>
      </div>
      <button type="submit" class="btn btn-sm mb-5 mx-auto d-block" style="color:white;width: 180px;background-color:#007bff">Save</button>
    </form>
    
</div>

@endsection
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>