@extends('layouts.layout')
    
@section('content')
<div class="container mt-2">
    <table class="table table-hover table-striped" style="background: white">
        <thead class="table" style="background: #1473E7">
            <tr>
                <th scope="col">SI</th>
                <th scope="col">Name</th>
                <th scope="col">Address</th>
                <th scope="col">Contact</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($farmers as $application)
            <tr>
                <th scope="row">{{ $application->id }}</th>
                <td>{{ $application->name }}</td>
                <td>{{ $application->address }}</td>
                <td>{{ $application->contact }}</td>
                <td>
                    <a href="/farmer_list/edit/{{ $application->id }}"    
                        name="title">
                        View  
                    </a>         
                </td>
            </tr>
                @endforeach
        </tbody>
    </table>
    {{ $farmers->links() }}
</div>

@endsection